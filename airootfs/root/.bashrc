#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s autocd

alias doas="doas --"
alias p="doas pacman"
alias ls="ls --color=auto"
alias la="ls -A"
alias bt="bluetoothctl"

export EDITOR=nvim

PS1='[\u@\h \W]\$ '

