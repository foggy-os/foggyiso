#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

read -r -p "Start GUI? [Y/n] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ || "$response" = "" ]]; then
  if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
  fi
fi
