all:
	doas mkarchiso -v -w ./ -o ../ ./
	doas rm -rf base._* efiboot.img iso* build* x86_64

noremove:
	doas mkarchiso -v -w ./ -o ../ ./

clean:
	doas rm -rf base._* efiboot.img iso* build* x86_64
